FL-S Series NOR Flash Memory 
============================

Infineon's FL-S serial Flash memory provides fast quad SPI NOR Flash memory with densities from 128 Mb to 1 Gb for high-performance embedded systems. The FL-S family is AEC-Q100 qualified and supports PPAP for automotive customers at extended temperature ranges of -40°C to +125°C.

The FL-S family brings read speeds in single, dual, and quad I/O modes up to 133 MHz SDR (single data rate), and up to 80 MHz DDR (double data rate) delivering read bandwidth of up to 80 Mbps. Industry-leading programming performance (up to 1.08 Mbps) speeds manufacturing throughput and lowers programming costs dramatically.

